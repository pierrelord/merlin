api = 2
core = 7.x
projects[drupal][type] = "core"
projects[drupal][version] = "7.15"

; Contrib Modules

;projects[addressfield][subdir] = "contrib"
;projects[addressfield][version] = "1.0-beta3"

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc3"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.0"

;projects[auto_nodetitle][subdir] = "contrib"
;projects[auto_nodetitle][version] = "1.0"

projects[backup_migrate][subdir] = "contrib"
projects[backup_migrate][version] = "2.2"

projects[calendar][subdir] = "contrib"
projects[calendar][version] = "3.4"

;projects[ccl][subdir] = "contrib"
;projects[ccl][version] = "1.3"

;projects[cdn][subdir] = "contrib"
;projects[cdn][version] = "2.4"

;projects[collapsiblock][subdir] = "contrib"
;projects[collapsiblock][version] = "1.0"

projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = "1.3"

;projects[commerce][subdir] = "contrib"
;projects[commerce][version] = "1.2"

;projects[commerce_paypal][subdir] = "contrib"
;projects[commerce_paypal][version] = "1.x-dev"

;projects[commerce_eway][subdir] = "contrib"
;projects[commerce_eway][version] = "1.0-beta2"

;projects[commerce_shipping][subdir] = "contrib"
;projects[commerce_shipping][version] = "2.0-beta1"

;projects[commerce_flat_rate][subdir] = "contrib"
;projects[commerce_flat_rate][version] = "1.0-beta1"

;projects[contact][subdir] = "contrib"
;projects[contact][version] = "2.x-dev"

projects[context][subdir] = "contrib"
projects[context][version] = "3.0-beta3"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.0"

;projects[custom_breadcrumbs][subdir] = "contrib"
;projects[custom_breadcrumbs][version] = "1.0-alpha1"

projects[date][subdir] = "contrib"
projects[date][version] = "2.5"

projects[delta][subdir] = "contrib"
projects[delta][version] = "3.0-beta9"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.2"

projects[diff][subdir] = "contrib"
projects[diff][version] = "2.0"

projects[ds][subdir] = "contrib"
projects[ds][version] = "2.x-dev"

;projects[easy_social][subdir] = "contrib"
;projects[easy_social][version] = "2.8"

projects[email][subdir] = "contrib"
projects[email][version] = "1.0"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.0-rc3"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.0-rc3"

projects[entityreference_prepopulate][subdir] = "contrib"
projects[entityreference_prepopulate][version] = "1.1"

;projects[facebook_comments][subdir] = "contrib"
;projects[facebook_comments][version] = "1.0-beta1"

;projects[facetapi][subdir] = "contrib"
;projects[facetapi][version] = "1.0-rc4"

;projects[faqfield][subdir] = "contrib"
;projects[faqfield][version] = "1.0"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0"

projects[field_collection][subdir] = "contrib"
projects[field_collection][version] = "1.0-beta4"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.1"

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.0-unstable5"

;projects[fitvids][subdir] = "contrib"
;projects[fitvids][version] = "1.6"

;projects[fivestar][subdir] = "contrib"
;projects[fivestar][version] = "2.0-alpha2"

;projects[galleria][subdir] = "contrib"
;projects[galleria][version] = "1.0-beta3"

;projects[geocoder][subdir] = "contrib"
;projects[geocoder][version] = "1.1"

;projects[geofield][subdir] = "contrib"
;projects[geofield][version] = "1.0"

;projects[geophp][subdir] = "contrib"
;projects[geophp][version] = "1.4"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1.2"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "1.0"

projects[link][subdir] = "contrib"
projects[link][version] = "1.0"

;projects[mailchimp][subdir] = "contrib"
;projects[mailchimp][version] = "2.4"

projects[media][subdir] = "contrib"
projects[media][version] = "2.0-unstable5"

projects[mediaelement][subdir] = "contrib"
projects[mediaelement][version] = "1.2"

projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "1.0-beta3"

projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][version] = "1.0-beta5"

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.3"

projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.0-alpha6"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "1.6"

;projects[mollom][subdir] = "contrib"
;projects[mollom][version] = "2.0"

projects[nodequeue][subdir] = "contrib"
projects[nodequeue][version] = "2.0-beta1"

;projects[office_hours][subdir] = "contrib"
;projects[office_hours][version] = "1.0"

projects[omega_tools][subdir] = "contrib"
projects[omega_tools][version] = "3.0-rc4"

;projects[openlayers][subdir] = "contrib"
;projects[openlayers][version] = "2.0-beta1"

;projects[page_title][subdir] = "contrib"
;projects[page_title][version] = "2.5"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.1"

;projects[plup][subdir] = "contrib"
;projects[plup][version] = "1.0-alpha1"

;projects[redirect][subdir] = "contrib"
;projects[redirect][version] = "1.0-beta4"

;projects[rpx][subdir] = "contrib"
;projects[rpx][version] = "2.1"

;projects[rules][subdir] = "contrib"
;projects[rules][version] = "2.1"

;projects[scheduler][subdir] = "contrib"
;projects[scheduler][version] = "1.0"

;projects[schemaorg][subdir] = "contrib"
;projects[schemaorg][version] = "1.0-beta3"

;projects[search_api][subdir] = "contrib"
;projects[search_api][version] = "1.1"

;projects[search_api_solr][subdir] = "contrib"
;projects[search_api_solr][version] = "1.0-rc2"

;projects[securesite][subdir] = "contrib"
;projects[securesite][version] = "2.x-dev"

projects[stringoverrides][subdir] = "contrib"
projects[stringoverrides][version] = "1.8"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0-rc1"

;projects[site_verify][subdir] = "contrib"
;projects[site_verify][version] = "1.0"

;projects[spamspan][subdir] = "contrib"
;projects[spamspan][version] = "1.1-beta1"

;projects[submitted_by][subdir] = "contrib"
;projects[submitted_by][version] = "1.x-dev"

projects[superfish][subdir] = "contrib"
projects[superfish][version] = "1.8"

;projects[terms_of_use][subdir] = "contrib"
;projects[terms_of_use][version] = "1.1"

;projects[timeperiod][subdir] = "contrib"
;projects[timeperiod][version] = "1.0-beta1"

projects[token][subdir] = "contrib"
projects[token][version] = "1.1"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.0"

projects[variable][subdir] = "contrib"
projects[variable][version] = "1.2"

projects[views][subdir] = "contrib"
projects[views][version] = "3.3"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.0-rc1"

projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "3.0"

;projects[votingapi][subdir] = "contrib"
;projects[votingapi][version] = "2.6"

projects[webform][subdir] = "contrib"
projects[webform][version] = "3.18"

;projects[workbench][subdir] = "contrib"
;projects[workbench][version] = "1.1"

;projects[workbench_moderation][subdir] = "contrib"
;projects[workbench_moderation][version] = "1.1"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.x-dev"

projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0-rc1"


; Contrib Themes
projects[omega][type] = "theme"
projects[omega][subdir] = "contrib"
projects[omega][version] = "3.1"


; Custom Themes

projects[beagle][type] = "theme"
projects[beagle][subdir] = "contrib"
projects[beagle][download][type] = "git"
projects[beagle][download][url] = "git@bitbucket.org:pierrelord/beagle.git"





; Libraries

;libraries[tinymce][download][type] = "get"
;libraries[tinymce][download][url] = "http://github.com/downloads/tinymce/tinymce/tinymce_3.5b2.zip"
;libraries[tinymce][directory_name] = "tinymce"

;libraries[colorbox][download][type] = "get"
;libraries[colorbox][download][url] = "http://jacklmoore.com/colorbox/colorbox.zip"
;libraries[colorbox][directory_name] = "colorbox"

;libraries[plupload][download][type] = "get"
;libraries[plupload][download][url] = "http://www.plupload.com/download.php"
;libraries[plupload][directory_name] = "plupload"

;libraries[galleria][download][type] = "get"
;libraries[galleria][download][url] = "http://galleria.io/static/galleria-1.2.7.zip"
;libraries[galleria][directory_name] = "galleria"

;libraries[jquery.cycle][download][type] = "get"
;libraries[jquery.cycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"
;libraries[jquery.cycle][directory_name] = "jquery.cycle"

;libraries[mediaelement][download][type] = "get"
;libraries[mediaelement][download][url] = "http://github.com/johndyer/mediaelement/zipball/master"
;libraries[mediaelement][directory_name] = "mediaelement"

;libraries[SolrPhpClient][download][type] = "get"
;libraries[SolrPhpClient][download][url] = "http://solr-php-client.googlecode.com/files/SolrPhpClient.r60.2011-05-04.tgz"
;libraries[SolrPhpClient][directory_name] = "SolrPhpClient"

;libraries[superfish][download][type] = "get"
;libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal/tarball/master"
;libraries[superfish][directory_name] = "superfish"

; Not needed because auto installed by fitvids.
;libraries[fitvids][download][type] = "get"
;libraries[fitvids][download][url] = "https://github.com/davatron5000/FitVids.js/zipball/master"
;libraries[fitvids][directory_name] = "fitvids"