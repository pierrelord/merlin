#!/bin/bash

# Rebuild a profile.
# Optional first argument specifies an alias.

# This profile
PROFILE=merlin

# First argument specifies alternate alias
if [ -z "$1" ]; then
  ALIAS=$PROFILE.loc
else
  ALIAS=$1
fi
echo "Rebuilding $ALIAS with $PROFILE"

# Remove everything which is not themes/custom, modules/custom, modules/features
rm -rf modules/contrib
rm -rf themes/contrib
rm -rf themes/kylne
rm -rf libraries

# Make and tidy
drush @$ALIAS cc all
drush make --working-copy --no-gitinfofile --no-core --contrib-destination=. $PROFILE.make .
drush @$ALIAS updatedb
drush @$ALIAS cc all
