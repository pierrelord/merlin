#!/bin/bash

# Reinstall a profile.
# Optional first argument specifies an alias.

# This profile
PROFILE=merlin

# First argument specifies alternate alias
if [ -z "$1" ]; then
  ALIAS=$PROFILE.loc
else
  ALIAS=$1
fi
echo "Reinstalling $ALIAS with $PROFILE"

# Install
drush @$ALIAS si $PROFILE
